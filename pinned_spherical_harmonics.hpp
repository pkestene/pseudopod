/* -*- indent-tabs-mode: t -*- */
#ifndef MATH_PINNED_SPHERICAL_HARMONICS
#define MATH_PINNED_SPHERICAL_HARMONICS
// � Alfredo A. Correa 2020

#include<cassert>
#include<cmath>

namespace math {

template<std::ptrdiff_t L, class T = double>
struct pinned_real_spherical_harmonics{
	using index_type = std::ptrdiff_t; using size_type = index_type;
	using element_type = T;
	static constexpr size_type size(){return L;} static constexpr auto ell_max = L - 1;
	constexpr pinned_real_spherical_harmonics(element_type x, element_type y, element_type z){
    //		auto const R = [&](auto ell, auto m)->double&{return W[ell-m][ell+m];};
    //		for(int l = 0; l != L; ++l) for(int m = -l; m <= l; ++m) R(l, m) = spherical_harmonic2(l, m, x, y, z);
#if 1
		if(L < 1) return;
		auto const R = [&](auto ell, auto m)->double&{return W[ell-m][ell+m];};
		R(0, 0) = M_2_SQRTPI/4;

		if(L < 2) return;
		auto const r2 = x*x + y*y; auto const R2 = r2 + z*z;
		R(1, 0) = sqrt(3/(4.*M_PI*R2))*z;
		for(index_type l = 2; l != size(); ++l)
			R(l,0)=(sqrt((4*l*l-1)/R2)*z*R(l-1,0)-(l-1)*sqrt((2*l+1)/(2*l-3.))*R(l-2,0))/l;

		for(index_type l = 1; l != size(); ++l){
			auto tmp = (sqrt(2*l*(2*l+1)*R2/((l+1)*(2*l-1)))*R(l-1, 0) - z*sqrt(2*l/(l+1.))*R(l, 0))/r2;
			R(l, +1) = tmp*x; R(l, -1) = tmp*y;
		}

		if(L < 3) return;
		for(index_type l = 2; l < size(); ++l)
			for(index_type m = 2; m != l+1; ++m){
				auto const norm1 = z*sqrt((l-m+1.)/((l+m)))/r2;
				auto const norm2 = sqrt((l+m-1)*(2*l+1)*R2/((l+m)*(2*l-1.)))/r2;
				R(l, +m) = norm2*(R(l-1, m-1)*x - R(l-1, 1-m)*y) + norm1*(R(l, 1-m)*y - R(l, m-1)*x);
				R(l, -m) = norm2*(R(l-1, m-1)*y + R(l-1, 1-m)*x) - norm1*(R(l, m-1)*y + R(l, 1-m)*x);
			}
#endif
	}
	constexpr pinned_real_spherical_harmonics(element_type theta, element_type phi)
		: pinned_real_spherical_harmonics(cos(phi)*sin(theta), cos(phi)*sin(theta), cos(theta)){}
	constexpr auto operator[](index_type ell) const{assert(ell<=ell_max); 
		struct{
			element_type const* base_; index_type m_max;
			auto& operator[](index_type m) const{assert(std::abs(m) <= m_max); return base_[-(2*L-1)*m];}
		} ret{&W[ell][ell], ell}; return ret;
	}
private:
	double W[2*L][2*L];// = {};
	constexpr pinned_real_spherical_harmonics(element_type cosq, element_type cosf, element_type sinf, bool){
	}
};

}

#ifdef PSEUDOPOD_UNIT_TEST
#include <catch2/catch.hpp>
#include <cmath>

TEST_CASE("Function math::spherical_harmonic", "[spherical_harmonic]") {

	double r = 0;
	for(double x = 0.1; x <100.; x+=1.){
		pinned_real_spherical_harmonics<4> RY{x, x-10.2, x+0.3};
		for(int L = 0; L != 4; ++L){
			for(int M = -L; M <= L; ++M){
				r += RY[L][M];//, x, x-10.2, x+0.3);
			}
		}
	}

}

TEST_CASE("not recommened pattern"){
	double r = 0;
	for(double x = 0.1; x <100.; x+=1.){
		for(int L = 0; L != 4; ++L) for(int M = -L; M <= L; ++M){
        r += pinned_real_spherical_harmonics<4>{x, x-10.2, x+0.3}[L][M];//, x, x-10.2, x+0.3);
      }
	}
}

#endif
#endif


